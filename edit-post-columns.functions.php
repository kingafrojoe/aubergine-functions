<?php
// Add to admin_init function
add_filter('manage_edit-homepage-images_columns', 'add_new_homepage_columns');
function add_new_homepage_columns($homepage_columns) {
		$new_columns['cb'] = '<input type="checkbox" />';
		$new_columns['title'] = _x('Image Name', 'column name');
		$new_columns['image'] = __('Image');
		$new_columns['author'] = __('Author');
		$new_columns['date'] = _x('Date', 'column name');
 
		return $new_columns;
	}
	
// Add to admin_init function
add_action('manage_homepage-images_posts_custom_column', 'manage_gallery_columns', 10, 2);
 
	function manage_gallery_columns($column_name, $id) {
		global $wpdb;
		switch ($column_name) {
 
		case 'image':
			// Get number of images in gallery
			$num_images = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_parent = {$id};"));
			echo get_the_post_thumbnail($post->ID, array(80,80));  
			break;
		default:
			break;
		} // end switch
	}	