<?php 

/**
 * Modify taxonomy query to show all products
 *
 * @param WP_Query $query
 * @return WP_Query
 */
function mw_limit_tax_posts_per_page( $query = '' ) {

	// Bail if not a taxonomy page
	if (  !is_tax() )
		return;

	// Tax gets all posts
	$query->set( 'posts_per_page', -1 );
}
add_action( 'pre_get_posts', 'mw_limit_tax_posts_per_page' );

