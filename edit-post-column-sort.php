<?php // Register the column as sortable
function type_column_register_sortable( $columns ) {
	$columns['type'] = 'type';
 
	return $columns;
}
add_filter( 'manage_edit-attendees_sortable_columns', 'type_column_register_sortable' );

function color_clauses_mike( $clauses, $wp_query ) { // tell WP how to sort by taxonomy terms
	global $wpdb;
 
	if ( isset( $wp_query->query['orderby'] ) && 'type' == $wp_query->query['orderby'] ) {
 
		$clauses['join'] .= <<<SQL
LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
SQL;
 
		$clauses['where'] .= " AND (taxonomy = 'type' OR taxonomy IS NULL)";
		$clauses['groupby'] = "object_id";
		$clauses['orderby']  = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC) ";
		$clauses['orderby'] .= ( 'ASC' == strtoupper( $wp_query->get('order') ) ) ? 'ASC' : 'DESC';
	}
 
	return $clauses;
}
add_filter( 'posts_clauses', 'color_clauses_mike', 10, 2 );

