<?php 

add_filter( 'manage_edit-client_columns', 'my_edit_client_columns' ) ;

function my_edit_client_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Reference' ),
		'date' => __( 'Date' ),
		'relevant' => __( 'Relevant To' ),
	);

	return $columns;
}

add_action( 'manage_client_posts_custom_column', 'my_manage_client_columns', 10, 2 );

function my_manage_client_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		
		/* If displaying the 'Relevant To' column. */
		case 'relevant' :

			/* Get the genres for the post. */
			$terms = get_the_terms( $post_id, 'relevant-to' );

			/* If terms were found. */
			if ( !empty( $terms ) ) {

				$out = array();

				/* Loop through each term, linking to the 'edit posts' page for the specific term. */
				foreach ( $terms as $term ) {
					$out[] = sprintf( '<a href="%s">%s</a>',
						esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'relevant-to' => $term->slug ), 'edit.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'relevant-to', 'display' ) )
					);
				}

				/* Join the terms, separating them with a comma. */
				echo join( ', ', $out );
			}

			/* If no terms were found, output a default message. */
			else {
				_e( 'Not tagged as relevant to anything' );
			}

			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

function restrict_client_by_relevance() {
		global $typenow;
		$post_type = 'client'; // change HERE
		$taxonomy = 'relevant-to'; // change HERE
		if ($typenow == $post_type) {
			$selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
			$info_taxonomy = get_taxonomy($taxonomy);
			wp_dropdown_categories(array(
				'show_option_all' => __("Show All {$info_taxonomy->label}"),
				'taxonomy' => $taxonomy,
				'name' => $taxonomy,
				'orderby' => 'name',
				'selected' => $selected,
				'show_count' => true,
				'hide_empty' => true,
			));
		};
	}

	add_action('restrict_manage_posts', 'restrict_client_by_relevance');

	function convert_id_to_term_in_query($query) {
		global $pagenow;
		$post_type = 'client'; // change HERE
		$taxonomy = 'relevant-to'; // change HERE
		$q_vars = &$query->query_vars;
		if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0) {
			$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
			$q_vars[$taxonomy] = $term->slug;
		}
	}

	add_filter('parse_query', 'convert_id_to_term_in_query');