<?php 
/* ===============================================
 #  Common functions
 # ===============================================
*/

//
//Global variables - could be used for social urls
//
$aub_urls = array(
				'facebook' 	=> '#fb',	
				'twitter' 	=> '#tw',
				'pintrest'	=> '#p'
			);
global $aub_urls;

//
// add more crunch images sizes
//
add_image_size('square', 250, 250, true);

//makes custom crunch sizes accessable in WordPress Media Library

add_filter( 'image_size_names_choose', 'my_custom_sizes' );
 
function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'square' => __( 'Square' ),
    ) );
}


//
// Tells you if a number is Odd or even
//
function is_odd( $int )
{
  return( $int & 1 );
}


//
// Optional Include different scripts and styles
//
add_action('template_redirect', 'them_js_head_load');
function them_js_head_load(){
#	wp_deregister_script('jquery');
#	wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js');
#	wp_register_style('jqueryuicss', get_bloginfo('template_url').'/uicss/jquery-ui-1.10.3.custom.css');

#	wp_enqueue_script('jquery');
#	wp_enqueue_style('jqueryuicss');
}

//
// Custom excerpt function
//
function excerpt_content($str, $maxlen) {
	if ( strlen($str) <= $maxlen ) return $str;

	$newstr = substr($str, 0, $maxlen);
	if ( substr($newstr,-1,1) != ' ' ) $newstr = substr($newstr, 0, strrpos($newstr, " "));

	return strip_tags($newstr);
}

//
// Return all images attached to a post
//
function attached_images( $post_ID, $size, $limit=-1, $exclude='' )
{
	$argsThumb = array(
			'numberposts'    => $limit,
			'order'          => 'ASC',
			'post_type'      => 'attachment',
			'post_parent'    => $post_ID,
			'post_mime_type' => 'image',
			'post_status'    => null,
			'exclude'		 => $exclude
	);
	$attachments = get_posts($argsThumb);

	$returnImg = array();
	if ($attachments) {
			$n = 0;
			foreach ($attachments as $attachment) {
				 $image = wp_get_attachment_image_src($attachment->ID, $size);
				 $full = wp_get_attachment_image_src($attachment->ID,'full');
				 $alt_text = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
				 $returnImg[$n]['url']=$image[0];
				 $returnImg[$n]['id']=$attachment->ID;
				 $returnImg[$n]['alt']= $alt_text;
				 $returnImg[$n]['title']= $attachment->post_title;
				 $returnImg[$n]['full']=$full[0];
				 $returnImg[$n]['width']=$image[1];
				 $returnImg[$n]['height']=$image[2];
			$n++;	 
			}
	}

return $returnImg;
}

//
// Get the thumbnail of a post
//
function get_thumb_url($post_id,$thumb_size)
{
	$post_thumbnail_id = get_post_thumbnail_id( $post_id );
	if($post_thumbnail_id):
		$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, $thumb_size);
		$alt_text = get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true);
		 $full = wp_get_attachment_image_src($post_thumbnail_id,'full');
		 
		$returnImg['title']= get_the_title($post_thumbnail_id);
		$returnImg['alt']= $alt_text;
		$returnImg['ID']= $post_thumbnail_id;
		$returnImg['full']=$full[0];
		
		$merge = array_merge($post_thumbnail_img, $returnImg);
		return $merge;
	else:
		return false;
	endif;
}

//
// get all image sizes of an attachment Id
//
function get_attachment_image($attachment_id)
{
	$imageSizes = array('thumbnail','square','medium','large','full');
	$returnImg = array();
	foreach($imageSizes as $size){
		if(!isset($returnImg['title'])) {
			$returnImg['title'] = get_the_title( $attachment_id );
		}
		$img 		            = wp_get_attachment_image_src( $attachment_id, $size );
		$returnImg[$size]	 	= $img;
	}
return $returnImg;
}

//
// Change the login logo
//
function my_custom_login_logo() {
	$header_image = get_stylesheet_directory().'/images/login-logo.png';
	$size = getimagesize($header_image);
	$header_image = get_stylesheet_directory_uri().'/images/login-logo.png';
	if($size) {
		$width  = $size[0];
		$height = $size[1];
		echo '	<style type="text/css">
				body.login {
					background: #fff;
				}
				#login {
					width: 440px !important;
					padding:60px 0 0 0 !important;
				}
				.login h1 a {display:none;}
			   .login h1 { 
			   		background-image:url(' . $header_image . ') !important;
					
			   		background-size: ' . $width . 'px ' . $height . 'px;
					width: ' . $width . 'px;
					height: ' . $height . 'px;
					margin: 0 auto 20px;
			   }
			   .login form{
					width: 60%;
					margin: 0 auto;  
			   }
			   .login #nav, .login #backtoblog{
				    width: 69%;
					margin: 0 auto;  
			   }
			   .login #nav a, .login #backtoblog a{
				   color:#111 !important;   
			   }
			   a:hover, a:active, a:focus{
				   color:#333 !important;   
			   }
    		</style>';
	}
}
add_action('login_head', 'my_custom_login_logo');

//
// generate a random string
//
function randomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

//
// Automaticly log a user in by their username
//
function auto_login( $user, $type='login' ) {
	ob_start();
		$username = $user;
		// log in automatically
		if ( !is_user_logged_in() ) {
			$user =  get_user_by($type, $username);//get_userdatabylogin( $username );
			$user_id = $user->ID;
			wp_set_current_user( $user_id, $username );
			wp_set_auth_cookie( $user_id );
			do_action( 'wp_login', $username );
		} 
	ob_end_clean();
}

//
// Stop subscribers accessing the dashboard
//
add_action( 'admin_head', 'restrict_dashboard' );
function restrict_dashboard()
{
	$redirect = get_bloginfo('url');//get_permalink( 148 );
	$user_info = wp_get_current_user();
	$user_level = $user_info->user_level;
	if((int)$user_level < 2)
	{
		$location = $redirect;
		?>
		<META HTTP-EQUIV="refresh" CONTENT="1; URL=<?php echo $location; ?>">
		<script>
		window.location('<?php echo $location; ?>');
		</script>
		<?php
		die('Logging you in....');
		exit;
	}
}

//
// show admin bar only for admins and editors
//
if (!current_user_can('edit_posts')) {// || !current_user_can('manage_options')) {
	add_filter('show_admin_bar', '__return_false');
}

//
// get current url
//
function currentUrl() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
return $pageURL;
}

//
// get current page name
//
function currentPageName() {
	return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}

//
// Add new column to admin area 'homeslides' is variable in function name
//
function homeslides_columns_head($defaults) {  
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;  
}
//
// add content to the new column 'homeslides' is variable in function name
//
function homeslides_columns_content($column_name, $post_ID) {  
    if ($column_name == 'featured_image') {
        $thumb = get_thumb_url($post_ID,'thumbnail');
		if ($thumb[0]) {  
            echo '<img src="' . $thumb[0] . '" />';  
        }  
    }  
}  
//add_filter('manage_homeslides_posts_columns', 'homeslides_columns_head', 10);
//add_action('manage_homeslides_posts_custom_column', 'homeslides_columns_content', 10, 2);
add_filter('manage_posts_columns', 'homeslides_columns_head', 10);
add_action('manage_posts_custom_column', 'homeslides_columns_content', 10, 2);
add_filter('manage_pages_columns', 'homeslides_columns_head', 10);
add_action('manage_pages_custom_column', 'homeslides_columns_content', 10, 2);

//
// Remove Yoast columns
//
function mw_remove_columns( $columns ) {

	// remove the Yoast SEO columns
	unset( $columns['wpseo-score'] );
	unset( $columns['wpseo-focuskw'] );

	return $columns;

}
add_filter ( 'manage_edit-post_columns', 'mw_remove_columns' ); // remove from posts...
add_filter ( 'manage_edit-page_columns', 'mw_remove_columns' ); // remove from pages...


//
// Add stuff to each widget, in this case odd or even
//
function my_filter_dynamic_sidebar_params($params){

    static $sidebar_widget_count = array();
    $sidebar_id = $params[0]["id"];
    if (! isset($sidebar_widget_count[$sidebar_id])){
        $sidebar_widget_count[$sidebar_id] = 0;
    }
    $before_widget = $params[0]['before_widget'];
    $class = $sidebar_widget_count[$sidebar_id] % 2 ? 
        "widget-odd" : "widget-even";
    $class .= " widget-index-" . $sidebar_widget_count[$sidebar_id];
    $class .= " widget-in-$sidebar_id";
    $before_widget = str_replace("class=\"", 
        "class=\"$class ", $before_widget);
    $params[0]['before_widget'] = $before_widget;
    $sidebar_widget_count[$sidebar_id]++;
    return $params;
}

add_filter("dynamic_sidebar_params", "my_filter_dynamic_sidebar_params");

// conditional function for finding if you're on a single custom post - change 'reading' to the custom post type
function is_custompostreading()
{
	$post_type = get_query_var('post_type');
	return $post_type == 'reading' ? true : false;
}

// use on pages to find if you are an a sub, sub-sub etc page of page with ID of x, eg if(is_tree(10)) { ... }
function is_tree($pid) {
	global $post;
	if ( ! is_page() )
		return false;
	if ( is_page( $pid ) )
		return true;
	$anc = get_post_ancestors( $post );
	if ( in_array( $pid, $anc ) )
		return true;
	return false;
}

// print_r any filters registered
// Use like this <?php print_filters_for( 'the_content' ); ? >
function print_filters_for( $hook = '' ) {
	global $wp_filter;
	if( empty( $hook ) || !isset( $wp_filter[$hook] ) )
		return;

	print '<pre>';
	print_r( $wp_filter[$hook] );
	print '</pre>';
}

//adds current browser to body tag as a class
add_filter('body_class','browser_body_class');
function browser_body_class($classes) {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

	if($is_lynx) $classes[] = 'lynx';
	elseif($is_gecko) $classes[] = 'gecko';
	elseif($is_opera) $classes[] = 'opera';
	elseif($is_NS4) $classes[] = 'ns4';
	elseif($is_safari) $classes[] = 'safari';
	elseif($is_chrome) $classes[] = 'chrome';
	elsif($is_IE) $classes[] = 'ie';
	else $classes[] = 'unknown';

	if($is_iphone) $classes[] = 'iphone';
	return $classes;
}

// remove WP emoji script and css from wp_head()
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// removes the "Install the WooThemes Updater plugin to get updates for your WooThemes plugins." message
remove_action( 'admin_notices', 'woothemes_updater_notice' );

/* ===============================================
 # \ Common functions
 # ===============================================
*/