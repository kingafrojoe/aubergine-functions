<?php

add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );
function my_deregister_javascript() {
  if ( !is_page('44') && !is_page('195') && !is_page('197')) {
    wp_deregister_script( 'contact-form-7' );
  }
}

add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
function my_deregister_styles() {
  if ( !is_page('44') && !is_page('195') && !is_page('197')) {
    wp_deregister_style( 'contact-form-7' );
  }
}

